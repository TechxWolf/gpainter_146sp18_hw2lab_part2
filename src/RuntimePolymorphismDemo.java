/**
 * Test class to illustrate polymorphic behavior at runtime
 *
 * @author gpainter@email.uscb.edu
 * @version 146sp18 HW2 Lab part 2
 */
public class RuntimePolymorphismDemo {

    public static void main(String[] args) {
  
        Speakable[] speakableObjects = new Speakable[5];

        // ...and to each array element, we assign a reference
        // to a newly instantiated object of each Animal subclass
        speakableObjects[0] = new Dog("Spike");
        speakableObjects[1] = new Cat("Tom");
        speakableObjects[2] = new Mouse("Jerry");
        speakableObjects[3] = new Fox("Todd");
        speakableObjects[4] = new Computer();

        /*
         Now, we will loop through the animals array... note that nowhere
         in the for loop do we EXPLICITLY refer to a dog, cat, or mouse object!
         */
        for (Speakable currentSpeakableObject : speakableObjects) {
            // if this object is an animal, conversely, if it is NOT
            // a COMPUTER...
            if( !(currentSpeakableObject instanceof Computer) )
            {
                //Display this objects's greeting via the toString method
                // ( defined in the Animal superclass)
                System.out.print(currentSpeakableObject.toString());
            
            
            
            }//end instanceof if 
            
            
            
            // Polymorphically call the speak() method for the specific type 
            // of animal -- the choice of speak() method is determined at
            // runtime (execution time), not compile time! Thus, we call 
            // this an example of RUNTIME polymorphism.
            currentSpeakableObject.speak();

            /*
             (NOTE: In contrast, COMPILE-TIME(a.k.a "static" ) polymorphism is just another 
              way of saying "method overloading". In RUNTIME ("dynamic" ) polymorphism, we're 
              not over-LOADING methods -- rather, we are over-RIDING them!)
             */
        } // end for 

    } // end method main

} // end test class RuntimePolymorphismDemo
