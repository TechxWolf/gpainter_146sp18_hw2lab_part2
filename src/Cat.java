/**
 * Cat class that implements the speak() method of the
 * abstract Animal superclass
 *
 * @author gpainter@email.uscb.edu
 * @version 146sp18 HW2 Lab part 2
 */
public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public void speak() {
        // What does a cat say?
        System.out.println("Meow!");

    } // end CONCRETE method speak

} // end CONCRETE class Cat
