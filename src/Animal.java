/**
 * Class definition for an abstract Animal -- it's abstract because
 * we want the animal to speak (when commanded to do so), but we don't
 * know exactly what TYPE of animal it will be, and so we don't know
 * what it will say! (The implementation of the speak() method
 * will be handled by various subclasses of Animal.)
 *
 * And because it is INCOMPLETE, an abstract class
 * CANNOT BE INSTANTIATED! Do not forget this, EVER!!
 *
 * @author gpainter@email.uscb.edu
 * @version 146sp18 HW2 Lab part 2
 */
public abstract class Animal implements Speakable {
    private String name;

    //public abstract void speak();
    public Animal ( String name){
    
    this.name = name;
    }//end constructor
    
    public String getName() {
    
    return name;
    
    }//end get
    
    @Override
    public String toString(){
        return String.format("My name is %s. ", getName());
    
    }//end override
    
    // we dont implement speakable method speak() here. We will do that
   // in the CONCRETE subclasses. But this means that this class must STILL be
   // abstract to avoid a compilation error 
            

} // end ABSTRACT class Animal
