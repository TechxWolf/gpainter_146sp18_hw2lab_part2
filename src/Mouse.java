/**
 * Mouse class that implements the speak() method of the
 * abstract Animal superclass
 *
 * @author gpainter@email.uscb.edu
 * @version 146sp18 HW2 Lab part 2
 */
public class Mouse extends Animal {

    public Mouse(String name) {
        super(name);
    }

    @Override
    public void speak() {
        // What does a mouse say?
        System.out.println("Squeak!");

    } // end CONCRETE method speak

} // end CONCRETE class Mouse
