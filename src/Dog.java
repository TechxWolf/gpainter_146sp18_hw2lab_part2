/**
 * Dog class that implements the speak() method of the
 * abstract Animal superclass
 *
 * @author gpainter@email.uscb.edu
 * @version 146sp18 HW2 Lab part 2
 */
public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void speak() {
        // What does a dog say?
        System.out.println("Woof!");

    } // end CONCRETE method speak

} // end CONCRETE class Dog
