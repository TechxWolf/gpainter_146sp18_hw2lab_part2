/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author gpainter
 */
public interface Speakable {
    
    //public abstract void speak();
    void speak();// non-defualtMethods are implicitly abstract and 
                 //public in interfaces
    
}// end interface Speakable
